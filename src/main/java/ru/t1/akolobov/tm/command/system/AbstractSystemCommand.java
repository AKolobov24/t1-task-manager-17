package ru.t1.akolobov.tm.command.system;

import ru.t1.akolobov.tm.api.service.ICommandService;
import ru.t1.akolobov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
