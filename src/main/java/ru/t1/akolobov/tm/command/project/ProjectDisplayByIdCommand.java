package ru.t1.akolobov.tm.command.project;

import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class ProjectDisplayByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-display-by-id";

    public static final String DESCRIPTION = "Find project by Id and display.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        displayProject(project);
    }

}
