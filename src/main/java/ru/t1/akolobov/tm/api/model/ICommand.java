package ru.t1.akolobov.tm.api.model;

public interface ICommand {

    String getName();

    String getArgument();

    String getDescription();

    void execute();

}
